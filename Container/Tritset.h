#ifndef TRIT_SET_H
#define TRIT_SET_H

#include <string>
#include <unordered_map>

enum class Trit {
    FALSE, UNKNOWN, TRUE = 3
};

typedef unsigned int uint;

uint tritBytesNeeded(uint count);

namespace TritSpace {

    class TritSet {

        class ReferenceImpl {
            friend class TritSet;
        public:
            ReferenceImpl &operator=(Trit tritValue);
            ReferenceImpl &operator=(const ReferenceImpl &rhs);
            const char *str() const;

        private:
            ReferenceImpl(TritSet *p, uint index);

            TritSet *parent;
            Trit currentTrit;
            uint currentTritIndex;
        };

        class IteratorImpl {
            friend class TritSet;
        public:
            IteratorImpl();
            IteratorImpl &operator++();
            IteratorImpl operator++(int);
            ReferenceImpl &operator*();
            bool operator==(const IteratorImpl &rhs);
            bool operator!=(const IteratorImpl &rhs);

        private:
            explicit IteratorImpl(TritSet *parent);
            ReferenceImpl ref;
        };

        class ConstIteratorImpl {
            friend class TritSet;
        public:
            ConstIteratorImpl();
            ConstIteratorImpl &operator++();
            ConstIteratorImpl operator++(int);
            Trit &operator*();
            bool operator==(const ConstIteratorImpl &rhs);
            bool operator!=(const ConstIteratorImpl &rhs);

        private:
            explicit ConstIteratorImpl(TritSet *parent);
            ReferenceImpl ref;
        };

    public:
        explicit TritSet(uint sz = 0);

        TritSet(const TritSet &other);

        ~TritSet();

        TritSet &operator=(const TritSet &other);

        typedef ReferenceImpl reference;

        typedef IteratorImpl iterator;

        typedef ConstIteratorImpl const_iterator;

        const TritSet operator&(const TritSet &rhs);

        const TritSet operator|(const TritSet &rhs);

        const TritSet operator~();

        uint size() const;

        void trim(uint lastIndex);

        uint lenght();

        void shrink();

        uint capacity() const;

        uint cardinality(Trit value);

        std::unordered_map<int, int, std::hash<int>> cardinality();

        reference operator[](uint index);

        Trit operator[](uint index) const;

        const_iterator cbegin();

        const_iterator cend();

        iterator begin();

        iterator end();

    private:
        class Impl;
        Impl *impl;
    };

}

#endif // !TRIT_SET_H