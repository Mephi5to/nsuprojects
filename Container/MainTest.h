#ifndef CONTAINER_MAINTEST_H_H
#define CONTAINER_MAINTEST_H_H


#include "gtest/gtest.h"
#include <fstream>

#include "Tritset.h"

using namespace TritSpace;

TEST(TestingTritSet, CapacityAllocation) {
    auto sizes = {
            0, 1, 2, 3, 4, 5, 6, 7,
            8, 9, 10, 11, 12, 13, 14,
            15, 16, 17, 18, 19, 20, 21,
            22, 23, 24, 25, 26, 27, 28,
            29, 30, 31, 32, 33, 34, 35,
            36, 40, 100, 1000, 1500, 2000,
            3500, 5700, 8000, 9000, 10000, 1010101
    };

    for (auto sz : sizes) {
        TritSpace::TritSet set(sz);
        EXPECT_EQ(tritBytesNeeded(sz), set.capacity());
    }
}

TEST(TestingTritSet, AccessingTest) {
    TritSet set(100);
    EXPECT_STREQ("Unknown", set[0].str());
    EXPECT_STREQ("Unknown", set[1].str());
    EXPECT_STREQ("Unknown", set[100].str());
    EXPECT_STREQ("Unknown", set[1000000].str());

    set[99] = Trit::TRUE;
    set[1000000] = Trit::TRUE;
    EXPECT_STREQ("True", set[99].str());
    EXPECT_STREQ("True", set[1000000].str());
}

TEST(TestingTritSet, TestCardinality) {
    TritSet set(1000);
    set[10000000] = Trit::TRUE;
    set[10] = Trit::TRUE;
    EXPECT_EQ(2, set.cardinality(Trit::TRUE));

    set[1] = Trit::FALSE;
    EXPECT_EQ(1, set.cardinality(Trit::FALSE));
}

TEST(TestingTritSet, TestCardinalityMap) {
    TritSet set(1000);
    set[0] = Trit::FALSE;
    set[1] = Trit::TRUE;
    set[10] = Trit::TRUE;
    set[2] = Trit::FALSE;
    set[3] = Trit::FALSE;
    EXPECT_EQ(2, set.cardinality()[static_cast<uint>(Trit::TRUE)]);
    EXPECT_EQ(3, set.cardinality()[static_cast<uint>(Trit::FALSE)]);
    EXPECT_EQ(6, set.cardinality()[static_cast<uint>(Trit::UNKNOWN)]);
}

TEST(TestingTritSet, TestSize) {
    TritSet set(100);
    EXPECT_EQ(100, set.size());
    TritSet arr(1000);
    EXPECT_EQ(1000, arr.size());
}

TEST(TestingTritSet, TestLenght) {
    TritSet set(1000);
    set[500] = Trit::TRUE;
    EXPECT_EQ(501, set.lenght());
    set[560] = Trit::TRUE;
    EXPECT_EQ(561, set.lenght());
}

TEST(TetstingTritSet, TestTrim) {
    TritSet set(1000);
    set[500] = Trit::TRUE;
    set[501] = Trit::TRUE;
    set[502] = Trit::TRUE;
    set[503] = Trit::TRUE;
    set[504] = Trit::TRUE;
    set.trim(501);
    EXPECT_STREQ("True", set[500].str());
    EXPECT_STREQ("True", set[501].str());
    EXPECT_STREQ("Unknown", set[502].str());
    EXPECT_STREQ("Unknown", set[503].str());
    EXPECT_STREQ("Unknown", set[504].str());
}

TEST(TetstingTritSet, TestShrink) {
    TritSet set(1000);
    set[450] = Trit::TRUE;
    set[504] = Trit::TRUE;
    set.shrink();
    EXPECT_STREQ("True", set[450].str());
    EXPECT_STREQ("True", set[504].str());
    EXPECT_EQ(tritBytesNeeded(504), set.capacity());
}

TEST(TestingTritSet, TestConjuction) {
    TritSet a(10);
    TritSet b(1000);
    TritSet c;
    a[0] = Trit::TRUE;
    a[1] = Trit::FALSE;
    b[1] = Trit::TRUE;
    b[2] = Trit::TRUE;
    b[3] = Trit::TRUE;
    c = a & b;
    EXPECT_STREQ("Unknown", c[0].str());
    EXPECT_STREQ("False", c[1].str());
    EXPECT_STREQ("Unknown", c[2].str());
    EXPECT_STREQ("Unknown", c[3].str());
}

TEST(TestingTritSet, TestDisjuntion) {
    TritSet a(10);
    TritSet b(1000);
    TritSet c;
    a[0] = Trit::TRUE;
    a[1] = Trit::FALSE;
    b[1] = Trit::TRUE;
    b[2] = Trit::TRUE;
    b[3] = Trit::TRUE;
    c = a | b;
    EXPECT_STREQ("True", c[0].str());
    EXPECT_STREQ("True", c[1].str());
    EXPECT_STREQ("True", c[2].str());
    EXPECT_STREQ("True", c[3].str());
    EXPECT_STREQ("Unknown", c[4].str());
}

TEST(TestingTritSet, TestInversion) {
    TritSet set(10);
    set[0] = Trit::TRUE;
    set[1] = Trit::FALSE;
    set = ~set;
    EXPECT_STREQ("False", set[0].str());
    EXPECT_STREQ("True", set[1].str());
    EXPECT_STREQ("Unknown", set[2].str());
}

TEST(TestingReferenceImpl, TestConjuction) {
    TritSet a(10);
    TritSet b(1000);
    TritSet c(20);
    a[0] = Trit::TRUE;
    a[1] = Trit::FALSE;
    b[1] = Trit::TRUE;
    b[2] = Trit::TRUE;
    b[3] = Trit::TRUE;
    c[0] = a[0] & b[2];
    c[1] = a[1] & b[2];
    c[10] = a[0] & b[100];
    c[16] = a[1] & b[0];
    EXPECT_STREQ("True", c[0].str());
    EXPECT_STREQ("False", c[1].str());
    EXPECT_STREQ("Unknown", c[10].str());
    EXPECT_STREQ("False", c[16].str());
}

TEST(TestingReferenceImpl, TestDisjuction) {
    TritSet a(10);
    TritSet b(1000);
    TritSet c(20);
    a[0] = Trit::TRUE;
    a[1] = Trit::FALSE;
    b[1] = Trit::TRUE;
    b[2] = Trit::TRUE;
    b[3] = Trit::TRUE;
    c[0] = a[0] | b[2];
    c[1] = a[1] | b[2];
    c[10] = a[0] | b[100];
    c[16] = a[1] | b[0];
    EXPECT_STREQ("True", c[0].str());
    EXPECT_STREQ("True", c[1].str());
    EXPECT_STREQ("True", c[10].str());
    EXPECT_STREQ("Unknown", c[16].str());
}

TEST(TestingReferenceImpl, TestInversion) {
    TritSet a(100);
    TritSet b(123);
    a[0] = Trit::FALSE;
    a[32] = Trit::TRUE;
    a[53] = Trit::FALSE;
    a[19] = Trit::TRUE;
    b[0] = ~a[19];
    b[32] = ~a[32];
    b[53] = ~a[53];
    b[19] = ~a[0];
    b[1] = ~a[67];
    EXPECT_STREQ("False", b[0].str());
    EXPECT_STREQ("False", b[32].str());
    EXPECT_STREQ("True", b[53].str());
    EXPECT_STREQ("True", b[19].str());
    EXPECT_STREQ("Unknown", b[1].str());
}

TEST(TestingIterator, Iterations) {
    TritSet set(10);
    set[0] = Trit::TRUE;
    set[1] = Trit::TRUE;
    set[10] = Trit::TRUE;
    TritSet::iterator it;
    for (it = set.begin(); it != set.end(); ++it) {
        *it = Trit::FALSE;
    }
    EXPECT_STREQ("False", set[0].str());
    EXPECT_STREQ("False", set[1].str());
    EXPECT_STREQ("False", set[10].str());
}

TEST(TestingIterator, ConstIterations) {
    TritSet set(10);
    TritSet::const_iterator it;
    for (it = set.cbegin(); it != set.cend(); ++it) {
        EXPECT_EQ(1, static_cast<uint>(*it));
    }
}

TEST(TestingTritSet, TestingStreams) {
    std::ofstream file("output.txt");

    TritSet set(100);
    TritSet newset;

    set[0] = Trit::TRUE;
    set[1] = Trit::FALSE;
    set[3] = Trit::TRUE;
    set[15] = Trit::TRUE;
    set[50] = Trit::TRUE;
    set[99] = Trit::FALSE;

    set.saveToStream(file);
    file.close();

    std::ifstream infile("output.txt");

    newset.loadFromStream(infile);

    infile.close();

    EXPECT_EQ(set.size(), newset.size());
    EXPECT_STREQ("True", newset[0].str());
    EXPECT_STREQ("False", newset[1].str());
    EXPECT_STREQ("True", newset[3].str());
    EXPECT_STREQ("True", newset[15].str());
    EXPECT_STREQ("True", newset[50].str());
    EXPECT_STREQ("False", newset[99].str());
}

#endif //CONTAINER_MAINTEST_H_H
