#include "Tritset.h"
#include <iostream>
#include <algorithm>

using namespace TritSpace;

constexpr uint BYTE_SIZE = 8;

constexpr uint END_FLAG = UINT_MAX;

constexpr uint tritHelpIndex = sizeof(uint) * 8 / 2;

inline uint tritBytesNeeded(uint count) {
    if (count % sizeof(uint) == 0) {
        return count * 2 / BYTE_SIZE / sizeof(uint);
    }
    return count * 2 / BYTE_SIZE / sizeof(uint) + 1;
};

constexpr uint DEFAULT_BYTE = 85;

uint dafaultValueAtStorage() {
    uint defByte = DEFAULT_BYTE;
    for (uint i = 0; i < sizeof(uint) - 1; ++i) {
        defByte <<= BYTE_SIZE;
        defByte += DEFAULT_BYTE;
    }
    return defByte;
}

class TritSet::Impl {
public:
    Impl() = default;

    ~Impl() {
        freeStorage();
    }

    Impl(const Impl &rhs) = delete;

    Impl &operator=(const Impl &) = delete;

    static const uint DEF_CELL;

    void allocateStorage(uint trits) {
        uint cap = tritBytesNeeded(trits);
        size = trits;
        capacity = cap;
        storage = new uint[capacity];
        for (uint i = 0; i < capacity; ++i) {
            storage[i] = DEF_CELL;
        }
    }

    void reallocateStorage(uint trits) {
        uint newCapacity = tritBytesNeeded(trits);
        auto *newStorage = new uint[newCapacity];

        for (uint i = 0; i < newCapacity; ++i) {
            newStorage[i] = DEF_CELL;
        }

        for (uint i = 0; i < capacity; ++i) {
            newStorage[i] = storage[i];
        }

        freeStorage();
        size = trits;
        capacity = newCapacity;
        storage = newStorage;
    }

    void freeStorage() {
        if (capacity <= 1) {
            delete storage;
        } else {
            delete[]storage;
        }
        size = capacity = 0;
        storage = nullptr;
    }

    void setTritValueAt(uint index, Trit tritVal) {
        auto tritValue = static_cast<uint>(tritVal);
        uint storageIndex = tritBytesNeeded(index);
        uint tritIndex = index % tritHelpIndex;
        uint offset = 2 * tritIndex;
        uint bytes = storage[storageIndex];
        uint write = tritValue << offset;
        uint newBytes = static_cast<uint>(~0) - (3 << offset);
        newBytes = bytes & newBytes;
        newBytes = newBytes | write;

        storage[storageIndex] = newBytes;
    }

    Trit getTritValueAt(uint index) const {
        uint storageIndex = tritBytesNeeded(index);
        uint tritIndex = index % tritHelpIndex;
        uint offset = 2 * tritIndex;
        uint bytes = storage[storageIndex];
        uint tritVal = (bytes & (3 << offset)) >> offset;

        return static_cast<Trit>(tritVal);
    }

    uint quantityOfTrue() {
        uint count = 0;
        for (uint i = 0; i < size; ++i) {
            if (Trit::TRUE == getTritValueAt(i)) {
                ++count;
            }
        }

        return count;
    }

    uint quantityOfFalse() {
        uint count = 0;
        for (uint i = 0; i < size; ++i) {
            if (Trit::FALSE == getTritValueAt(i)) {
                ++count;
            }
        }

        return count;
    }

    uint quantityOfUnknown() {
        uint count = 0;
        uint realCount = 0;
        Trit value;
        for (uint i = 0; i < size; ++i) {
            value = getTritValueAt(i);
            if (value == Trit::UNKNOWN) {
                ++count;
            }
            if (value == Trit::FALSE || value == Trit::TRUE) {
                realCount = count;
            }
        }

        return realCount;
    }

    uint size = 0;
    uint capacity = 0;
    uint *storage = nullptr;
};

const uint TritSet::Impl::DEF_CELL = dafaultValueAtStorage();

TritSet::TritSet(uint sz)
        : impl(new Impl) {
    if (sz < 0) {
        throw std::runtime_error("Out of Range error: TritSet");
    }
    impl->allocateStorage(sz);
}

TritSet::~TritSet() {
    delete impl;
}

TritSet::TritSet(const TritSet &other) {
    impl->capacity = other.impl->capacity;
    impl->size = other.impl->size;
    impl->storage = new uint[impl->capacity];

    for (uint i = 0; i < impl->capacity; ++i) {
        impl->storage[i] = other.impl->storage[i];
    }
}

TritSet &TritSet::operator=(const TritSet &other) {
    impl->freeStorage();
    impl->capacity = other.impl->capacity;
    impl->size = other.impl->size;
    impl->storage = new uint[impl->capacity];

    for (uint i = 0; i < impl->capacity; ++i) {
        impl->storage[i] = other.impl->storage[i];
    }

    return *this;
}

TritSet::reference TritSet::operator[](uint index) {
    return reference(this, index);
}

Trit TritSet::operator[](uint index) const {
    return impl->getTritValueAt(index);
}

TritSet::iterator TritSet::begin() {
    return iterator(this);
}

TritSet::iterator TritSet::end() {
    auto e = iterator(this);
    e.ref.currentTritIndex = END_FLAG;
    return e;
}

uint TritSet::size() const {
    return impl->size;
}

uint TritSet::capacity() const {
    return impl->capacity;
}

uint TritSet::cardinality(Trit value) {
    switch (value) {
        case Trit::TRUE :
            return impl->quantityOfTrue();
        case Trit::FALSE :
            return impl->quantityOfFalse();
        case Trit::UNKNOWN :
            return impl->quantityOfUnknown();
    }
}

const TritSet TritSet::operator&(const TritSet &rhs) {
    uint maxSize = rhs.impl->size < impl->size ? impl->size : rhs.impl->size;
    uint minSize = rhs.impl->size > impl->size ? impl->size : rhs.impl->size;
    TritSet c(maxSize);

    for (uint i = 0; i < minSize; ++i) {
        c.impl->storage[i] = rhs.impl->storage[i] & impl->storage[i];
    }

    if (rhs.impl->capacity < impl->capacity) {
        for (uint i = minSize; i < maxSize; ++i) {
            c.impl->storage[i] = impl->storage[i] & Impl::DEF_CELL;
        }
    }

    if (rhs.impl->capacity > impl->capacity) {
        for (uint i = minSize; i < maxSize; ++i) {
            c.impl->storage[i] = rhs.impl->storage[i] & Impl::DEF_CELL;
        }
    }

    return c;
}

const TritSet TritSet::operator|(const TritSet &rhs) {
    uint maxSize = rhs.impl->size < impl->size ? impl->size : rhs.impl->size;
    uint minSize = rhs.impl->size > impl->size ? impl->size : rhs.impl->size;
    TritSet c(maxSize);

    for (uint i = 0; i < minSize; ++i) {
        c.impl->storage[i] = rhs.impl->storage[i] | impl->storage[i];
    }

    if (rhs.impl->capacity < impl->capacity) {
        for (uint i = minSize; i < maxSize; ++i) {
            c.impl->storage[i] = impl->storage[i] | Impl::DEF_CELL;
        }
    }

    if (rhs.impl->capacity > impl->capacity) {
        for (uint i = minSize; i < maxSize; ++i) {
            c.impl->storage[i] = rhs.impl->storage[i] | Impl::DEF_CELL;
        }
    }

    return c;
}

const TritSet TritSet::operator~() {
    TritSet c(impl->size);
    c.impl->size = impl->size;
    c.impl->capacity = impl->capacity;
    Trit tritValue;

    for (uint i = 0; i < impl->size; ++i) {
        tritValue = impl->getTritValueAt(i);
        if (tritValue == Trit::UNKNOWN) {
            continue;
        }
        if (tritValue == Trit::TRUE) {
            c.impl->setTritValueAt(i, Trit::FALSE);
        }
        if (tritValue == Trit::FALSE) {
            c.impl->setTritValueAt(i, Trit::TRUE);
        }
    }

    return c;
}

void TritSet::trim(uint lastIndex) {
    uint newCapacity = tritBytesNeeded(lastIndex);

    auto *newStorage = new uint[newCapacity];

    for (int i = 0; i < newCapacity; ++i) {
        newStorage[i] = impl->storage[i];
    }

    for (uint i = lastIndex + 1; i < tritHelpIndex * tritBytesNeeded(lastIndex); ++i) {
        impl->setTritValueAt(i, Trit::UNKNOWN);
    }

    impl->freeStorage();
    impl->storage = newStorage;
    impl->capacity = newCapacity;
    impl->size = lastIndex + 1;
}

std::unordered_map<int, int, std::hash<int>> TritSet::cardinality() {
    std::unordered_map<int, int, std::hash<int>> map(3);
    map[static_cast<int>(Trit::TRUE)] = impl->quantityOfTrue();
    map[static_cast<int>(Trit::FALSE)] = impl->quantityOfFalse();
    map[static_cast<int>(Trit::UNKNOWN)] = impl->quantityOfUnknown();
    return map;
}

TritSet::const_iterator TritSet::cbegin() {
    return const_iterator(this);
}

TritSet::const_iterator TritSet::cend() {
    auto e = const_iterator(this);
    e.ref.currentTritIndex = END_FLAG;
    return e;
}

uint TritSet::lenght() {
    uint count = 0;
    uint lght = 0;
    Trit value;

    for (uint i = 0; i < impl->size; ++i) {
        value = impl->getTritValueAt(i);
        ++count;
        if (value == Trit::FALSE || value == Trit::TRUE) {
            lght = count;
        }
    }

    return lght;
}

void TritSet::shrink() {
    uint lght = lenght();
    uint tritIndex = lght > 0 ? lght - 1 : 0;

    trim(tritIndex);
}


TritSet::ReferenceImpl::ReferenceImpl(TritSet *p, uint index)
        : parent(p), currentTritIndex(index), currentTrit(Trit::UNKNOWN) {
    if (parent != nullptr && p->impl->size > index) {
        currentTrit = p->impl->getTritValueAt(index);
    }
}

const char *TritSet::ReferenceImpl::str() const {
    auto value = currentTrit;
    if (parent->impl->capacity > 0) {
        value = parent->impl->getTritValueAt(currentTritIndex);
    }
    switch (value) {
        case Trit::TRUE:
            return "True";
        case Trit::FALSE:
            return "False";
        case Trit::UNKNOWN:
            return "Unknown";
    }
}

TritSet::ReferenceImpl &TritSet::ReferenceImpl::operator=(Trit tritValue) {
    if ((tritValue == Trit::UNKNOWN || tritValue == Trit::FALSE) && parent->impl->size < currentTritIndex) {
        return *this;
    }

    uint parentCapacity = parent->impl->capacity;
    if (parentCapacity == 0) {
        parent->impl->allocateStorage(currentTritIndex);
    }

    uint supposeCap = tritBytesNeeded(currentTritIndex);
    if (supposeCap > parentCapacity) {
        parent->impl->reallocateStorage(currentTritIndex);
    }

    currentTrit = tritValue;
    parent->impl->setTritValueAt(currentTritIndex, currentTrit);

    return *this;
}

TritSet::ReferenceImpl &TritSet::ReferenceImpl::operator=(const ReferenceImpl &rhs) {
    if (&rhs == this) {
        return *this;
    }

    if ((rhs.currentTrit == Trit::UNKNOWN || rhs.currentTrit == Trit::FALSE) && parent->impl->size < currentTritIndex) {
        return *this;
    }

    uint parentCapacity = parent->impl->capacity;
    if (parentCapacity == 0) {
        parent->impl->allocateStorage(currentTritIndex);
    }
    uint supposeCap = tritBytesNeeded(currentTritIndex);
    if (supposeCap > parentCapacity) {
        parent->impl->reallocateStorage(currentTritIndex);
    }

    currentTrit = rhs.currentTrit;
    currentTritIndex = rhs.currentTritIndex;

    return *this;
}

TritSet::IteratorImpl::IteratorImpl()
        : ref(nullptr, 0) {
}

TritSet::IteratorImpl::IteratorImpl(TritSet *parent)
        : ref(parent, 0) {
}

TritSet::IteratorImpl TritSet::IteratorImpl::operator++(int) {
    iterator iter(ref.parent);
    iter.ref.currentTritIndex = ref.currentTritIndex;

    if (ref.currentTritIndex + 1 < ref.parent->impl->size) {
        ref = reference(ref.parent, ref.currentTritIndex + 1);
    } else {
        ref.currentTritIndex = END_FLAG;
    }

    return iter;
}

TritSet::IteratorImpl &TritSet::IteratorImpl::operator++() {
    if (ref.currentTritIndex + 1 < ref.parent->impl->size) {
        ref = reference(ref.parent, ref.currentTritIndex + 1);
    } else {
        ref.currentTritIndex = END_FLAG;
    }
    return *this;
}

bool TritSet::IteratorImpl::operator==(const IteratorImpl &rhs) {
    return ref.parent == rhs.ref.parent && ref.currentTritIndex == rhs.ref.currentTritIndex;
}

bool TritSet::IteratorImpl::operator!=(const IteratorImpl &rhs) {
    return !(*this == rhs);
}

TritSet::ReferenceImpl &TritSet::IteratorImpl::operator*() {
    return ref;
}

TritSet::ConstIteratorImpl::ConstIteratorImpl()
        : ref(nullptr, 0) {
}

TritSet::ConstIteratorImpl &TritSet::ConstIteratorImpl::operator++() {
    if (ref.currentTritIndex + 1 < ref.parent->impl->size) {
        ref = reference(ref.parent, ref.currentTritIndex + 1);
    } else {
        ref.currentTritIndex = END_FLAG;
    }
    return *this;
}

TritSet::ConstIteratorImpl TritSet::ConstIteratorImpl::operator++(int) {
    const_iterator iter(ref.parent);
    iter.ref.currentTritIndex = ref.currentTritIndex;

    if (ref.currentTritIndex + 1 < ref.parent->impl->size) {
        ref = reference(ref.parent, ref.currentTritIndex + 1);
    } else {
        ref.currentTritIndex = END_FLAG;
    }

    return iter;
}

Trit &TritSet::ConstIteratorImpl::operator*() {
    return ref.currentTrit;
}

bool TritSet::ConstIteratorImpl::operator==(const ConstIteratorImpl &rhs) {
    return ref.parent == rhs.ref.parent && ref.currentTritIndex == rhs.ref.currentTritIndex;
}

bool TritSet::ConstIteratorImpl::operator!=(const ConstIteratorImpl &rhs) {
    return !(*this == rhs);
}

TritSet::ConstIteratorImpl::ConstIteratorImpl(TritSet *parent)
        : ref(parent, 0) {
}